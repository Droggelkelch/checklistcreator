from operator import mod
from tokenize import String
from xml.etree.ElementTree import tostring
from reportlab.pdfgen import canvas
from xml.dom import minidom
import sys
from reportlab.lib.colors import red, black, white
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4



def pageheader():
    c.setFont("Helvetica", fontsize_remark)
    c.setFillColor(red)
    c.drawCentredString(10.5 * cm, y_header_remark, cl_header_remark)

    c.setFillColor(black)
    c.setFont("Helvetica", fontsize_ident)
    c.drawCentredString(10.5 * cm, y_ident, cl_ident)

def pagefooter():
    c.setFont("Helvetica", fontsize_item)
    c.setFillColor(black)

    if pageno % 2 == 0:   #gerade seite
        c.drawString(x,y_footer,cl_page_prefix+"-"+str(pageno))
    else:                 #ungerade seite
        c.drawRightString(x_right_border, y_footer,cl_page_prefix+"-"+str(pageno))

        if renderstage == 'build':   ##-- register malen
            c.setFont('Helvetica', fontsize_register)
            y_reg = y_start_register
            c.line(21*cm - register_width,y_reg,21*cm,y_reg)
            y_reg=y_reg - 1.25*fontsize_register
            for eintrag in register:
                index_regzeile = 0
                for wort in eintrag['zeilen']:
                    c.drawCentredString(register_middle,y_reg,wort)
                    y_reg=y_reg - fontsize_register
                    index_regzeile = index_regzeile + 1
                while index_regzeile < register_maxlines:
                    y_reg = y_reg - fontsize_register
                    index_regzeile = index_regzeile + 1
                y_reg=y_reg - 0.25*fontsize_register
                c.drawCentredString(register_middle,y_reg,eintrag['prefix']+"-"+str(eintrag['page_start']))
                y_reg=y_reg - 0.45*fontsize_register
                c.line(21*cm - register_width,y_reg,21*cm,y_reg)
                y_reg = y_reg - 1.25*fontsize_register



    c.setFont("Helvetica", fontsize_item)
    c.drawCentredString(10.5*cm,y_footer,"Change "+cl_change_version)
    
if (1 < len(sys.argv)):
    filename = sys.argv[1]
    file = minidom.parse(filename)
    print('XML-File: ' + filename)
    newfilename = filename.split("\\")[-1].split(".")
else:
    print("call the programm with an XML-File to parse")
    quit()

def print_caution(y, text):
    y = y - 0.25 * linespacing
    c.setFont("Helvetica-Bold", fontsize_item)
    c.setFillColor(black)
    c.drawCentredString(10.5*cm,y,"CAUTION")

    textwidth = c.stringWidth("CAUTION", "Helvetica-Bold", fontsize_item)
    c.setStrokeColor(black)
    c.rect(10.5*cm-0.5*textwidth-0.25*cm, y - 0.25*cm, textwidth + 0.5*cm, fontsize_item + 0.5*cm)
    c.rect(10.5*cm-0.5*textwidth-0.35*cm, y - 0.35*cm, textwidth + 0.7*cm, fontsize_item + 0.7*cm)

    y = y - linespacing
    c.setFont("Helvetica", fontsize_item)
    c.drawCentredString(10.5*cm,y,text)

def print_note(y, text):
    c.setFont("Helvetica-Bold", fontsize_item)
    c.setFillColor(black)
    c.drawCentredString(10.5*cm,y,"NOTE")
    
    y = y - linespacing
    c.setFont("Helvetica", fontsize_item)
    c.drawCentredString(10.5*cm,y,text)

def print_list(y, nodelist, pageno, boxflag, list_startitem):
    #print("        print_list(): len(nodelist)="+str(len(nodelist))+", list_startitem="+str(list_startitem))
    c.setFillColor(black)
    alpha = 0
    li_index = 0
    for listitem in nodelist:     
        if listitem.nodeType == minidom.Node.TEXT_NODE:
            li_index = li_index + 1
            continue
        if li_index < list_startitem:
            alpha = alpha + 1
            li_index = li_index + 1
            continue
        if listitem.tagName == "entry":
            #print("            print_list: li_index="+str(li_index)+", item="+listitem.attributes['item'].value)
            if y < y_body_end or (listitem.hasAttribute('caution') and y < y_body_end + 3.25*linespacing) or (listitem.hasAttribute('annotation') and y < y_body_end + 1.75*linespacing):
                pagefooter()
                c.showPage()
                pageno = pageno + 1
                pageheader()
                y = y_start
                if boxflag == 1:
                    ## zu box-tag zurückgeben
                    return y, pageno, li_index
            
            c.setFont("Helvetica", fontsize_item)
            c.drawString(x+2*indent_item,y,listenbezeichner[alpha]+")")
            indent_points = c.stringWidth(listitem.attributes['item'].value,"Helvetica", fontsize_item)
            width_point = c.stringWidth(".","Helvetica", fontsize_item)
            c.drawString(x+2*indent_item+indent_item_space,y,listitem.attributes['item'].value)
            pointspace = indent_item_setting - 2*indent_item - indent_item_space - indent_points
            no_points = int( pointspace // width_point)
            outstring = " ".ljust(no_points - 1, ".")
            c.drawString(x+2*indent_item+indent_item_space+indent_points,y,outstring)
            if listitem.hasAttribute('annotation'):
                c.drawString(x+indent_item_setting,y,listitem.firstChild.data+"*")
                y = y - 0.75*linespacing
                c.drawString(x+2*indent_item+indent_item_space,y,"*"+listitem.attributes['annotation'].value)
            else:
                c.drawString(x+indent_item_setting,y,listitem.firstChild.data)
            alpha = alpha + 1
            y = y - linespacing
            if listitem.hasAttribute('caution'):
                print_caution(y, listitem.attributes['caution'].value)
                y = y - 2.25 * linespacing

        if listitem.tagName == "note":
            if y - 2 * linespacing < y_body_end:
                pagefooter()
                c.showPage()
                pageno = pageno + 1
                pageheader()
                y = y_start
                if boxflag == 1:
                    ## zu box-tag zurückgeben
                    return y, pageno, li_index

            print_note(y, listitem.firstChild.data)
            y = y - 2 * linespacing
            
        li_index = li_index + 1

    return y, pageno, li_index


# parameter settings
x = 3*cm
x_right_border = 18*cm
register_width = 2.5 * cm
register_middle = 21*cm - register_width/2
y_header_remark = 28*cm
y_ident = 27*cm
y_start = 25.5*cm
y_body_end = 3.5*cm
y_footer = 1.5*cm
y_title = 21*cm
y_space_subtitle = 3*cm
y_start_register = 27*cm
indent_procedure = 1.5*cm
indent_item = 1*cm
indent_item_space = 1*cm
indent_item_setting = 10.5*cm
fontsize_remark = 16
fontsize_ident = 16
fontsize_procedure = 16
fontsize_item = 13
fontsize_title = 44
fontsize_subtitle = 30
fonsize_register = 10
linespacing = 1*cm
linespace_before_procedure = 0.3 * cm
linespace_before_subtitle = 0.2 * cm
boxtitle_offsetbelow = 0.15*cm
boxtitle_height = fontsize_item+0.25*cm
offsetboxbelow = 0.7*cm

register = []
renderstage = ''
register_maxlines = 3
listenbezeichner = "abcdefghijklmnopqrstuvwxyz"
chapterno = 1

chklist = file.getElementsByTagName('checkliste')

cl_ident = chklist[0].attributes['ident'].value
cl_header_remark = chklist[0].attributes['header-remark'].value
cl_change_version = chklist[0].attributes['change-version'].value
cl_authority = chklist[0].attributes['authority'].value 
cl_organisation = chklist[0].attributes['organisation'].value 

for stage in ['prebuild', 'build']:             #-- prebuild=seitenzahlen etc. ermitteln, build=finales dokument rendern
    if stage == 'prebuild':
        renderstage = 'prebuild'
        c=canvas.Canvas(newfilename[0]+"_prebuild.pdf", pagesize=A4)
    else:
        renderstage = 'build'
        c=canvas.Canvas(newfilename[0]+".pdf", pagesize=A4)

    y=y_start
    chapterno = 0

    #----------------------------------------------------------------------------------------------Frontpage
    c.setFont("Helvetica", fontsize_remark)
    c.setFillColor(red)
    c.drawCentredString(10.5 * cm, y_header_remark, cl_header_remark)

    c.setFillColor(black)
    c.setFont("Helvetica", fontsize_ident)
    c.drawString(x, y_start, cl_organisation)
    c.drawRightString(x_right_border, y_start, cl_ident)

    y=y_title
    c.setFont("Helvetica-Bold", fontsize_title)
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['title1'].value)
    y = y - fontsize_title
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['title2'].value)
    y = y - fontsize_title
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['title3'].value)

    y = y - y_space_subtitle
    c.setFont("Helvetica-Bold", fontsize_subtitle)
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['subtitle1'].value)
    y=y-fontsize_subtitle
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['subtitle2'].value)
    y=y-fontsize_subtitle
    c.drawCentredString(10.5*cm, y, chklist[0].attributes['subtitle3'].value)

    y=y-1*cm
    c.line(x,y,x_right_border,y)

    y=y_body_end + 2* fontsize_ident + 1*cm
    c.line(x,y,x_right_border,y)
    c.setFont("Helvetica", fontsize_ident)
    y=y_body_end + fontsize_ident
    c.drawCentredString(10.5*cm,y,"PUBLISHED UNDER THE AUTHORITY OF THE")
    y=y-fontsize_ident
    c.drawCentredString(10.5*cm,y,cl_authority)


    c.showPage()

    #----------------------------------------------------------------------------------------------General
    #----------------------------------------------------------------------------------------------Normal Procedures
    pageno = 1
    y=y_start
    pageheader()
    normal = file.getElementsByTagName('normal')
    cl_page_prefix = normal[0].attributes['page-prefix'].value

    procedures = normal[0].getElementsByTagName('procedure')

    for procedure in procedures:
        chapterno = chapterno + 1
        
        if y < y_body_end + 2* linespacing + linespace_before_procedure:
            pagefooter()
            c.showPage()
            pageno = pageno + 1
            pageheader()
            y = y_start
        else:
            y = y - linespace_before_procedure

        c.setFont("Helvetica-Bold", fontsize_procedure)
        c.drawString(x,y,str(chapterno)+".")
        c.drawString(x+indent_procedure,y,procedure.attributes['name'].value)

        if stage == 'prebuild':
            chapter_lines = procedure.attributes['name'].value.split(" ")
            if len(chapter_lines) > register_maxlines:
                register_maxlines = len(chapter_lines)
            for wort in chapter_lines:
                fontsize_reg = fonsize_register
                while register_width < c.stringWidth(wort,"Helvetica", fontsize_reg):
                    fontsize_reg = fontsize_reg - 1
                fontsize_register = fontsize_reg
            register.append({
                "chapterno": chapterno,
                "zeilen": chapter_lines,
                "prefix": cl_page_prefix,
                "page_start": pageno,
                "page_end": 0
            })
            if y == y_start:    ##-- chapter begint auf der ersten zeile der seite
                register[chapterno - 2]['page_end'] = pageno - 1
            else:
                register[chapterno - 2]['page_end'] = pageno

        y = y - linespacing
        index = 0

        for item in procedure.childNodes:     
            if item.nodeType == minidom.Node.TEXT_NODE:
                continue

            if item.tagName == "newline":
                y = y - linespacing

            if item.tagName == "entry":
                if y < y_body_end or (item.hasAttribute('caution') and y < y_body_end + 3.25*linespacing) or (item.hasAttribute('annotation') and y < y_body_end + 1.75*linespacing):
                    pagefooter()
                    c.showPage()
                    pageno = pageno + 1
                    pageheader()
                    y = y_start

                index = index + 1
                c.setFont("Helvetica", fontsize_item)
                c.drawString(x+indent_item,y,str(index)+".")
                indent_points = c.stringWidth(item.attributes['item'].value,"Helvetica", fontsize_item)
                width_point = c.stringWidth(".","Helvetica", fontsize_item)
                c.drawString(x+indent_item+indent_item_space,y,item.attributes['item'].value)
                pointspace = indent_item_setting - indent_item - indent_item_space - indent_points
                no_points = int( pointspace // width_point)
                outstring = " ".ljust(no_points - 1, ".")
                c.drawString(x+indent_item+indent_item_space+indent_points,y,outstring)
                if item.hasAttribute('annotation'):
                    c.drawString(x+indent_item_setting,y,item.firstChild.data+"*")
                    y = y - 0.75*linespacing
                    c.drawString(x+indent_item+indent_item_space,y,"*"+item.attributes['annotation'].value)
                else:
                    c.drawString(x+indent_item_setting,y,item.firstChild.data)
                y = y - linespacing
                if item.hasAttribute('caution'):
                    print_caution(y, item.attributes['caution'].value)
                    y = y - 2.25 * linespacing
                continue

            
            if item.tagName == "subtitle":
                if y - linespace_before_subtitle - linespacing < y_body_end:
                    pagefooter()
                    c.showPage()
                    pageno = pageno + 1
                    pageheader()
                    y = y_start + linespace_before_subtitle

                c.setFont("Helvetica-Bold", fontsize_item)
                c.drawString(x+indent_item,y-linespace_before_subtitle,item.firstChild.data)
                y = y - linespacing
                continue

            if item.tagName == "list":
                y_new, pageno_new, lastlistitem = print_list(y, item.childNodes, pageno, 0, 0)
                y=y_new
                pageno=pageno_new
                continue
                        
            if item.tagName == "note":
                if y - 2 * linespacing < y_body_end:
                    pagefooter()
                    c.showPage()
                    pageno = pageno + 1
                    pageheader()
                    y = y_start

                print_note(y, item.firstChild.data)
                y = y - 2 * linespacing
                continue
            
            if item.tagName == "box":
                if y - 2 * linespacing < y_body_end:            ## abfangen, if firstChild has caution or annotation or is note
                    pagefooter()
                    c.showPage()
                    pageno = pageno + 1
                    pageheader()
                    y = y_start

                boxitems = item.childNodes
                startboxitem = 0
                
                while startboxitem < len(boxitems) -1:
                    #print("while: startboxitem="+str(startboxitem)+", len(boxitems)="+str(len(boxitems)))
                    c.setStrokeColor(black)
                    c.setFillColor(black)
                    c.rect(x,y-boxtitle_offsetbelow,10.5*cm-x,boxtitle_height, 1,1)
                    c.setFillColor(white)
                    c.setFont("Helvetica-Bold", fontsize_item)
                    c.drawString(x,y," "+item.attributes['title'].value)

                    y = y - linespacing
                    ind_boxitem = 0
                    y_boxende = y

                    ##-- restliche erforderliche länge ermitteln / größer seitenende?
                    for boxitem in boxitems:
                        if ind_boxitem < startboxitem:
                            ind_boxitem = ind_boxitem + 1
                            continue
                        if boxitem.nodeType == minidom.Node.TEXT_NODE:
                            ind_boxitem = ind_boxitem + 1
                            continue

                        if boxitem.tagName == "entry":
                            y_boxende = y_boxende - linespacing
                            if boxitem.hasAttribute('annotation'):
                                y_boxende = y_boxende - 0.75*linespacing
                            if boxitem.hasAttribute('caution'):
                                y_boxende = y_boxende - 2.25 * linespacing
                        if boxitem.tagName == "note":
                            y_boxende = y_boxende - 2*linespacing
                        if boxitem.tagName == "list":
                            listitems = boxitem.childNodes
                            for listitem in listitems:
                                if listitem.nodeType == minidom.Node.TEXT_NODE:
                                    continue

                                if listitem.tagName == "entry":
                                    y_boxende = y_boxende - linespacing
                                    if listitem.hasAttribute('annotation'):
                                        y_boxende = y_boxende - 0.75*linespacing
                                    if listitem.hasAttribute('caution'):
                                        y_boxende = y_boxende - 2.25 * linespacing
                                if listitem.tagName == "note":
                                    y_boxende = y_boxende - 2*linespacing
                        ind_boxitem = ind_boxitem + 1

                    
                    c.setStrokeColor(black)
                    c.setFillGray(0.75)
                    if y_boxende < y_body_end:
                        ##-- seitenumbruch erforderlich, box bis ende seite zeichnen
                        c.rect(x,y+linespacing-boxtitle_offsetbelow,x_right_border-x,y_body_end-(y+linespacing-boxtitle_offsetbelow), 1,1)
                    else:
                        c.rect(x,y+linespacing-boxtitle_offsetbelow,x_right_border-x,y_boxende-(y+linespacing-boxtitle_offsetbelow)+offsetboxbelow, 1,1)

                    ind_boxitem = 0
                    for boxitem in boxitems:
                        #print("    For: ind_boxitem="+str(ind_boxitem)+", Type="+str(boxitem.nodeType))
                        if ind_boxitem < startboxitem:
                            ind_boxitem = ind_boxitem + 1
                            continue
                        if boxitem.nodeType == minidom.Node.TEXT_NODE:
                            ind_boxitem = ind_boxitem + 1
                            continue
                        
                        if boxitem.tagName == "entry":
                            #print("        Entry: item="+boxitem.attributes['item'].value)
                            if y < y_body_end or (boxitem.hasAttribute('caution') and y < y_body_end + 3.25*linespacing) or (boxitem.hasAttribute('annotation') and y < y_body_end + 1.75*linespacing):
                                pagefooter()
                                c.showPage()
                                pageno = pageno + 1
                                pageheader()
                                y = y_start
                                startboxitem = ind_boxitem
                                break

                            index = index + 1
                            c.setFont("Helvetica", fontsize_item)
                            c.setFillColor(black)
                            c.drawString(x+indent_item,y,str(index)+".")
                            indent_points = c.stringWidth(boxitem.attributes['item'].value,"Helvetica", fontsize_item)
                            width_point = c.stringWidth(".","Helvetica", fontsize_item)
                            c.drawString(x+indent_item+indent_item_space,y,boxitem.attributes['item'].value)
                            pointspace = indent_item_setting - indent_item - indent_item_space - indent_points
                            no_points = int( pointspace // width_point)
                            outstring = " ".ljust(no_points - 1, ".")
                            c.drawString(x+indent_item+indent_item_space+indent_points,y,outstring)
                            if boxitem.hasAttribute('annotation'):
                                c.drawString(x+indent_item_setting,y,boxitem.firstChild.data+"*")
                                y = y - 0.75*linespacing
                                c.drawString(x+indent_item+indent_item_space,y,"*"+boxitem.attributes['annotation'].value)
                            else:
                                c.drawString(x+indent_item_setting,y,boxitem.firstChild.data)
                            y = y - linespacing
                            if boxitem.hasAttribute('caution'):
                                print_caution(y, boxitem.attributes['caution'].value)
                                y = y - 2.25 * linespacing

                        if boxitem.tagName == "note":
                            if y - 2 * linespacing < y_body_end:
                                pagefooter()
                                c.showPage()
                                pageno = pageno + 1
                                pageheader()
                                y = y_start
                                startboxitem = ind_boxitem
                                break

                            print_note(y, boxitem.firstChild.data)
                            y = y - 2 * linespacing

                        if boxitem.tagName == "list":
                            list_startitem = 0
                            while list_startitem < len(boxitem.childNodes) - 1:
                                y_new, pageno_new, list_lastitem = print_list(y, boxitem.childNodes, pageno, 1, list_startitem)
                                y=y_new
                                if pageno_new > pageno: ## seitenwechsel innerhalb print_list erfolgt
                                    #print("--- Seitenwechsel in print_list() erfolgt ---")
                                    pageno=pageno_new
                                    list_startitem = list_lastitem
                                    ## neuen titel malen
                                    c.setStrokeColor(black)
                                    c.setFillColor(black)
                                    c.rect(x,y-boxtitle_offsetbelow,10.5*cm-x,boxtitle_height, 1,1)
                                    c.setFillColor(white)
                                    c.setFont("Helvetica-Bold", fontsize_item)
                                    c.drawString(x,y," "+item.attributes['title'].value)
                                    
                                    y = y - linespacing
                                    y_boxende = y
                                    
                                    ##verbleibenden platzbedarf liste ermitteln
                                    listindex = 0
                                    #print("            platzbedarf restliste ermitteln: list_startitem="+str(list_startitem))
                                    for listitem in boxitem.childNodes:
                                        #print("            listindex="+str(listindex))
                                        if listindex < list_startitem:
                                            listindex = listindex + 1
                                            continue
                                        if listitem.nodeType == minidom.Node.TEXT_NODE:
                                            listindex = listindex + 1
                                            continue
                                        listindex = listindex + 1
                                        if listitem.tagName == "entry":
                                            #print("                platzbedarf restliste: listindex="+str(listindex)+", item="+listitem.attributes['item'].value)
                                            y_boxende = y_boxende - linespacing
                                            if listitem.hasAttribute('annotation'):
                                                y_boxende = y_boxende - 0.75*linespacing
                                            if listitem.hasAttribute('caution'):
                                                y_boxende = y_boxende - 2.25 * linespacing
                                        if listitem.tagName == "note":
                                            y_boxende = y_boxende - 2*linespacing
                                    
                                    ##verbleibenden platzbedarf verbleibende boxitems ermitteln

                                    ind_boxitem1 = 0

                                    for boxitem1 in boxitems:
                                        if ind_boxitem1 < ind_boxitem + 1:
                                            ind_boxitem1 = ind_boxitem1 + 1
                                            continue
                                        if boxitem1.nodeType == minidom.Node.TEXT_NODE:
                                            ind_boxitem1 = ind_boxitem1 + 1
                                            continue
                                        ind_boxitem1 = ind_boxitem1 + 1

                                        if boxitem1.tagName == "entry":
                                            y_boxende = y_boxende - linespacing
                                            if boxitem1.hasAttribute('annotation'):
                                                y_boxende = y_boxende - 0.75*linespacing
                                            if boxitem1.hasAttribute('caution'):
                                                y_boxende = y_boxende - 2.25 * linespacing
                                        if boxitem1.tagName == "note":
                                            y_boxende = y_boxende - 2*linespacing
                                        if boxitem1.tagName == "list":
                                            listitems1 = boxitem1.childNodes
                                            for listitem1 in listitems1:
                                                if listitem1.nodeType == minidom.Node.TEXT_NODE:
                                                    continue

                                                if listitem1.tagName == "entry":
                                                    y_boxende = y_boxende - linespacing
                                                    if listitem1.hasAttribute('annotation'):
                                                        y_boxende = y_boxende - 0.75*linespacing
                                                    if listitem1.hasAttribute('caution'):
                                                        y_boxende = y_boxende - 2.25 * linespacing
                                                if listitem1.tagName == "note":
                                                    y_boxende = y_boxende - 2*linespacing
                    
                                    c.setStrokeColor(black)
                                    c.setFillGray(0.75)
                                    if y_boxende < y_body_end:
                                        ##-- seitenumbruch erforderlich, box bis ende seite zeichnen
                                        c.rect(x,y+linespacing-boxtitle_offsetbelow,x_right_border-x,y_body_end-y, 1,1)
                                    else:
                                        c.rect(x,y+linespacing-boxtitle_offsetbelow,x_right_border-x,y_boxende-(y+linespacing-boxtitle_offsetbelow)+offsetboxbelow, 1,1)
                                    
                                list_startitem = list_lastitem
                                #print("            end-while: list_startitem="+str(list_startitem))

                        ind_boxitem = ind_boxitem + 1
                    if ind_boxitem == len(boxitems) -1:
                        startboxitem = len(boxitems) -1
                    startboxitem = ind_boxitem

    pagefooter()

    if y == y_start:    ##-- chapter begint auf der ersten zeile der seite
        register[chapterno - 1]['page_end'] = pageno - 1
    else:
        register[chapterno - 1]['page_end'] = pageno



    #----------------------------------------------------------------------------------------------Emergency Procedures

    emergency = file.getElementsByTagName('emergency')

    c.showPage()

    #---------------------------------------------------------------------------------------------Dokument aufbereiten fertig
    if stage == 'prebuild':
        y = y_start
        for eintrag in register:
            c.setFont('Helvetica', fontsize_register)
            c.drawString(x,y,str(eintrag['chapterno']))
            y=y-fontsize_register
            for wort in eintrag['zeilen']:
                c.drawString(x,y,"   "+wort)
                y=y-fontsize_register
            c.drawString(x,y,str(eintrag['page_start'])+" .. "+str(eintrag['page_end']))
            y=y-2*fontsize_register
        c.drawString(x,y,"Fontsize: "+str(fontsize_register))
        c.showPage()
    #----------------------------------------------------------------------------------------------PDF schreiben
c.save()
print('Created PDF Checklist: '+newfilename[0]+".pdf")