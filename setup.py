import sys
from cx_Freeze import setup, Executable

build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup( name = "CreateCL",
        version = "0.9",
        description = "Tool zum Erstellen von vGAF Checklisten",
        options = { "build_exe": build_exe_options},
        executables = [Executable("createcl.py", base=base)])
